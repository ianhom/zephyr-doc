﻿# 《Zephyr Project 文档 - 中文版》

- 本分支是 Zephyr v1.7.0 版的中文版

## 招募翻译志愿者：

- Zephyr v1.7.0 预计会在明年 2 月底 3 月初发布，到时会增加一些协议栈相关的内容，希望能有 Zephyr 爱好者一起翻译。
- 基本要求：
  - 英语：无要求，词典谁不会呀。
  - Git/GitHub：最好会，不过关系也不大

- 如果愿意无私奉献，先报个名吧。

## 报名

- 报名基本流程：Fork --> Clone 到本地  --> 修改本文件 --> Add --> Commit --> Push --> 提交 Pull Request。
- 具体路程请参考 [CONTRIBUTION.md](CONTRIBUTION.md)。

**报名列表**：
- [tidyjiang8](https://github.com/tidyjiang8)
- [ianhom](https://github.com/ianhom)
- [Jxzx](https://github.com/Jxzx)  

## 学习交流

- QQ 学习交流群： 580070214
